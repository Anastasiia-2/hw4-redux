export const getStateFromLS = keyName => {
  const data = localStorage.getItem(keyName);
  if (!data) return;
  return JSON.parse(data);
};

export const setStateToLS = (keyName, state) => {
  localStorage.setItem(keyName, JSON.stringify(state));
};

export const resetLS = () => {
  localStorage.clear();
};
