import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import { shallowEqual, useSelector } from "react-redux";

export default function CartPage() {
  const cartItems = useSelector(state => state.cart.cartItems, shallowEqual);
  return cartItems.length ? (
    <ItemsContainer items={cartItems} />
  ) : (
    <p>"You have not added anything to your cart yet..."</p>
  );
}
