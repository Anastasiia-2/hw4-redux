import React from "react";
import { MdClose } from "react-icons/md";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { AiOutlineStar, AiFillStar } from "react-icons/ai";
import { BsCartPlus } from "react-icons/bs";
import styles from "./Item.module.scss";
import { toggleFavouriteItem } from "../../redux/items/actionCreators";
import {
  setModalCallback,
  setModalHeader,
  setModalText,
  toggleModal,
} from "../../redux/modal/actionCreators";
import { useLocation } from "react-router-dom";
import { addToCart, removeFromCart } from "../../redux/cart/actionCreators";

export default function Item({ item }) {
  const { src, price, name, id } = item;
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  const favourites = useSelector(state => state.items.favItems, shallowEqual);
  const isFavourited = !!favourites?.find(el => el.id === id);

  const handleFavBtnClick = () => {
    dispatch(toggleFavouriteItem(item));
  };

  const handleAddToCart = () => {
    dispatch(toggleModal());
    dispatch(setModalHeader("Add to cart"));
    dispatch(setModalText("Do you want to add this product to your cart?"));
    dispatch(setModalCallback(() => dispatch(addToCart(item))));
  };

  const handleRemoveFromCart = () => {
    dispatch(toggleModal());
    dispatch(setModalHeader("Remove from cart"));
    dispatch(
      setModalText("Do you want to remove this product from your cart?")
    );
    dispatch(setModalCallback(() => dispatch(removeFromCart(item))));
  };

  return (
    <article className={styles.item}>
      <div className={styles.itemImg}>
        <img src={src} alt={name} />

        {pathname === "/cart" ? (
          <button onClick={handleRemoveFromCart} className={styles.closeButton}>
            {<MdClose />}
          </button>
        ) : (
          <button onClick={handleAddToCart} className={styles.itemAddToCartBtn}>
            {<BsCartPlus size="24" />}
          </button>
        )}

        <button
          onClick={handleFavBtnClick}
          className={isFavourited ? styles.favorite : styles.itemAddToFavBtn}
        >
          {isFavourited ? (
            <AiFillStar size="24" className={styles.icon} />
          ) : (
            <AiOutlineStar size="24" className={styles.icon} />
          )}
        </button>
      </div>
      <div className={styles.textWrapper}>
        <p className={styles.itemTitle}>{name}</p>
        <p className={styles.itemPrice}> {price}</p>
      </div>
    </article>
  );
}
